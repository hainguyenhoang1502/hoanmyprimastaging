$(document).ready(function() {
    $('.slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,

        dots: true,
        arrows: false,
        responsive: [{
                breakpoint: 821,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            }, {
                breakpoint: 940,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            }
        ]
    });
    AOS.init();
    jQuery('#dateOfBirth').datetimepicker({
        i18n: {
            de: {
                months: [
                    'Januar', 'Februar', 'März', 'April',
                    'Mai', 'Juni', 'Juli', 'August',
                    'September', 'Oktober', 'November', 'Dezember',
                ],
                dayOfWeek: [
                    "So.", "Mo", "Di", "Mi",
                    "Do", "Fr", "Sa.",
                ]
            }
        },
        timepicker: false,
        format: 'd/m/Y'
    });
    jQuery('#bookingTime').datetimepicker({
        format: 'd/m/Y H:m'
    });
});
$(document).click(function(e) {
    if (!$(e.target).is('.dropdown__list') && $('.dropdown__list').has(e.target).length === 0) {
        $('.collapse').collapse('hide');
    }
});

$(window).scroll(function() {
    var navbar = $('.header');
    var scrollPosition = $(window).scrollTop();

    if (scrollPosition > 0) {
        navbar.addClass('scrolled');
    } else {
        navbar.removeClass('scrolled');
    }
});